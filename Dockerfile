FROM alpine:3.14

LABEL org.opencontainers.image.authors="Andrew A. Tasso" \
      org.opencontainers.image.description="A sample project for demonstrating docker in a production environment" \
      org.opencontainers.image.licenses="MIT" \
      org.opencontainers.image.vendor="Andrew A. Tasso" \
      org.label-schema.vendor="Andrew A. Tasso" \
      org.label-schema.license="MIT"

# We'll likely need to add SSL root certificates
RUN apk --no-cache add ca-certificates

CMD ["/bin/echo", "Hello world"]
